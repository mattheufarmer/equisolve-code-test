This code is for the code test portion of an Equisolve job application.

The goal is this:
```
Use PHP to output the lowest 10 positive integers where the sum of the digits of each integer equals 10 
and contains the number 7 as one of the digits.
```

The answer that is output is:
```
37
73
127
172
217
271
307
370
703
712
```
