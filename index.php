<?php
/**
 * Equisolve Application Code Test
 *
 * Goal: Use PHP to output the lowest 10 positive integers where the 
 * sum of the digits of each integer equals 10 and contains the number 
 * 7 as one of the digits.
 */

/**
 * Require the number class file
 */
require( 'number.php' );

/**
 * The number of target integers found
 * @var integer
 */
$count = 0;

/**
 * The list of target integers
 * @var array
 */
$answers = array();


for( $x = 0; $count < 10; $x++ ) {
    /**
     * Make sure the number is positive
     */
    if ( ! Number::is_positive( $x ) ) {
        continue;
    }
    
    /**
     * Make sure the number has a seven as one of its digits
     */
    if ( ! Number::has_seven( $x ) ) {
        continue;
    }
    
    /**
     * Make sure the digits add up to ten
     */
    if ( ! Number::digits_equal_ten( $x ) ) {
        continue;
    }

    /** 
     * If we've made it here, add the number to the answer list and
     * increment the count
     */
    $answers[] = $x;
    $count++;
}

/**
 * Echo the answers, each on their own line
 */
foreach( $answers as $answer ) {
    echo "$answer<br>";
}