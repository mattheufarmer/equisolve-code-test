<?php

class Number
{
    /**
     * Checks that the number includes seven as one of its digits
     * @param  integer $number The number being checked
     * @return boolean         True if seven exists
     *                         False if seven does not exist
     */
    public static function has_seven( int $number )
    {
        $string = "$number";
        return ( strpos( $string, '7' ) !== false ) ? true : false;
    }

    /**
     * Checks to see if the digits of the number add up to ten
     * @param  integer $number The number being checked
     * @return boolean          True if they add up to ten
     *                          False if they do not add up to ten
     */
    public static function digits_equal_ten( int $number )
    {
        $total = 0;
        $string = "$number";
        $array = str_split( $string );
        foreach ( $array as $digit ) {
            $total += intval( $digit );
        }
        return ( $total === 10 ) ? true : false;
    }

    /**
     * Checks that the number is positive
     * @param  integer $number The number being checked
     * @return boolean         True if positive
     *                         False if negative
     */
    public static function is_positive( int $number )
    {
        return ( $number > 0 ) ? true : false;
    }
}